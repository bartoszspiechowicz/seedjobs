String basePath = 'teams/unicorns'

freeStyleJob("$basePath/seed-test-jobs1") {
    displayName 'Seed Job 1'
    description 'Seed Job 1'

    scm {
        git {
            remote {
                name 'origin'
                url "git@bitbucket.org:bartoszspiechowicz/seedjobs.git"
                branch 'master'
                credentials 'bitbucket-credentials'
            }
        }
    }

    logRotator {
        numToKeep(10)
    }

    steps {
		dsl {
			external 'seed2.groovy'
		}
    }
}